# Mysite

This project is a basic Django application having a db which contains some data related to People and the Company they work in. The project supports http requests like GET, PUT and POST. One can update and retrieve the information of the project's database.

## Getting Started

After you clone the project, go to the downloads directory from the terminal and run the commands mentioned in the 'Installing' section below.

### Prerequisites

Softwares needed:  
Postman  
Django and Python installed in terminal  

### Installing

First make the required migrations-
```
python manage.py makemigrations
python manage.py migrate

```
Then run the project-
```
python manage.py runserver

```
You can also mention a particular port number if you want (in case the default port is busy)-

```
python manage.py runserver 8086

```

## Running the tests

To run the project in web browser, type the following in url (take care of the port number):

```
http://127.0.0.1:8000/hello/polls/PersonView/
```
```
http://127.0.0.1:8000/hello/polls/CompanyView/
```

To run the project as an admin in the web browser (for a better view of the database), you need to create a superuser:


```
python manage.py createsuperuser

```
The terminal will now ask for login and password. Create them and use it to login after running the following code in the browser-

```
http://127.0.0.1:8000/admin
```
After this, you will have a better view of the database, from where you can easily add, update and delete data from the db.

### In Postman

Open Postman and select GET. In the textbox, put the url: 

```
http://127.0.0.1:8000/hello/polls/PersonView/
```
and hit the Send button.

It will give you a better view of the database.

Similarly, use POST for inserting data and PUT for updating data in the db. (Basic knowledge of json format needed)

## Authors

* **Bhairavee Bawane** [Repository](https://bitbucket.org/bhairavee96/)

