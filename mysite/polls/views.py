from __future__ import unicode_literals
from django.shortcuts import render
from django.views.generic import View
from django.http import HttpResponse, QueryDict
import json
from django.core.exceptions import ObjectDoesNotExist
from polls.models import Person
from polls.models import Comp

class PersonView(View):
	def __init__(self):
		print 'Person View Working'

	def get(self, request, *args, **kwargs):
		_params = request.GET
		#persons = Person.objects.exclude(first_name=first_name) #it will print every name except that given in url
		#persons = Person.objects.filter(first_name=first_name) #it will print only that name given in url
		persons = Person.objects.all()
		#company = Comp.objects.all()
		#data = [persons, company]
		#return HttpResponse(data, content_type='application/json')
		return HttpResponse(persons, content_type='application/json')

	def put(self, request, *args, **kwargs):
		_arg1 = QueryDict(request.body)
		first_name = _arg1.get('first_name')
		last_name = _arg1.get('last_name')
		user_name = _arg1.get('user_name')
		company_name = _arg1.get('company')

		try:
			c = Comp.objects.get(company=company_name)
		except ObjectDoesNotExist:
			return HttpResponse("Put error")

		p = Person.objects.get(user_name=user_name)
		p.company=c
		p.first_name = first_name
		p.save()
		return HttpResponse("Updated")

	def post(self, request, *args, **kwargs):
		_arg2 = request.POST
		#import pdb; pdb.set_trace()
		first_name = _arg2.get('first_name')
		last_name = _arg2.get('last_name')
		user_name = _arg2.get('user_name')
		company_name = _arg2.get('company')

		try:
			get_company = Comp.objects.get(company=company_name)
		except ObjectDoesNotExist:
			return HttpResponse("Post error")

		try:
			get_user_name = Person.objects.get(user_name=user_name)
		except ObjectDoesNotExist:
			per = Person(first_name=first_name,last_name=last_name,user_name=user_name,company=get_company)
			per.save()
			return HttpResponse("Person's data inserted")
		return HttpResponse("User name already exists")

class CompanyView(View):
	def __init__(self):
		print 'Company View working'

	def get(self, request, *args, **kwargs):
		_params=request.GET
		company = Comp.objects.all()
		return HttpResponse(company, content_type='application/json')
	
	def post(self, request, *args, **kwargs):
		_arg3 = request.POST
		company_name = _arg3.get('company')
		location_name = _arg3.get('location')
		pin_code_name = _arg3.get('pin_code')

		try:
			get_company_name = Comp.objects.get(company=company_name)
		except ObjectDoesNotExist:
			c = Comp(company=company_name,location=location_name,pin_code=pin_code_name)
			c.save()
			return HttpResponse("Company's data inserted")
		return HttpResponse("Company name already exists")

	def put(self, request, *Args, **kwargs):
		_arg4 = QueryDict(request.body)
		company_name=_arg4.get('company')
		location_name=_arg4.get('location')
		pin_code_name=_arg4.get('pin_code')

		try:
			c=Comp.objects.get(company=company_name)
		except ObjectDoesNotExist:
			return HttpResponse("Could not find company")

		c.location=location_name
		c.pin_code=pin_code_name
		c.save()
		return HttpResponse("Company View Updated")

