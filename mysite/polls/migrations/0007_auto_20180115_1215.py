# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-01-15 12:15
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0006_auto_20180112_0918'),
    ]

    operations = [
        migrations.AlterField(
            model_name='comp',
            name='company',
            field=models.CharField(blank=True, max_length=20, null=True, unique=True),
        ),
    ]
