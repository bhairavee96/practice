from __future__ import unicode_literals
from models import Person
from models import Comp
from django.contrib import admin

admin.site.register(Person)
admin.site.register(Comp)