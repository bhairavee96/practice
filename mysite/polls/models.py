from __future__ import unicode_literals
from django.db import models

class Comp(models.Model):
	company = models.CharField(max_length=20, null=True, blank=True, unique=True)
	location = models.CharField(max_length=25, null=True, blank=True)
	pin_code = models.CharField(max_length=8, null=True, blank=True)
	
	def __unicode__(self):
		return str(self.company) + '\n'

class Person(models.Model):
	first_name = models.CharField(max_length=255)
	last_name = models.CharField(max_length=2, null=True, blank=True)
	user_name = models.CharField(max_length=255, unique=True)
	#salary = models.CharField(max_length=10, null=True, blank=True) #migrations made after removing this attribute
	company = models.ForeignKey(Comp,on_delete = models.CASCADE)

	def __unicode__(self):
		return str(self.first_name) + ' ' + str(self.last_name) + '\n'  