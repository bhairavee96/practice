from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt
from polls.views import PersonView, CompanyView

urlpatterns = [
    url(r'^polls/PersonView/$', csrf_exempt(PersonView.as_view())),
    url(r'^polls/CompanyView/$', csrf_exempt(CompanyView.as_view())),
]